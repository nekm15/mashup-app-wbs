const express = require('express');
const app= express();
app.set('port', process.env.PORT || 1337);


app.use(express.static('./cover'));

app.use(function(reg, res) {
    res.type('text/plain');
    res.status(404);
    res.send('404 - Seite nicht gefunden');

});

app.use(function(reg, res, next) {
    console.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.send('500 - Internal Error');

});

exports.start = function() {
    app.listen(app.get('port'), function() {
        console.log('Server hochgefahren auf Port: ' + app.get('port'));
    });
}